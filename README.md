# Crypto widget

---

Widget is used to display several currency exchanges rates and it allows the user to exchange USD for Crypto.

## Getting started

---

Install Node js from https://nodejs.org/en/download/

Clone the repository with command

`git clone https://gitlab.com/r.kurbanov9688/crypto_widget.git`

Go to the project folder and install the dependencies with command

`npm install`

Run project with command

`npm start`

Open http://localhost:3000 with browser to see the result.

## Technologies

---

- React
- Redux toolkit
- Material UI
- json server

## .env variables

REACT_APP_BASE_URL is environment variable which stores base url from the fake API

## JSON SERVER

---

Json server is used as a fake API. To connect to the real API, it's necessary to update the cryptoApi.js file.

## Available Scripts

In the project directory, it's possible to run:

### `npm run start:server`

Starts server on http://localhost:3004

### `npm run start:client`

Starts application on http://localhost:3000

### `npm start`

Starts the application and server simultaneously

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
App is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once `eject` is executed, it's impossible to go back!**

If something is wrong with the build tool and configuration choices, it's possible to `eject` at any time. This command will remove the single build dependency from project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into project so it have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts, so it's possible to tweak them.

It is not necessary to ever use `eject`. The curated feature set is suitable for small and middle deployments, and it's not obligatory to use this feature.
