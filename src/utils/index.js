/**
 * sorting array of objects
 * @param array {array} - array to sort
 * @param comparator {function} - function helper
 * @returns {array}
 */
export const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
};

/**
 * function helper for comparator
 * @param a {string | number} - value to compare
 * @param b {string | number} - value to compare
 * @param orderBy {string} - label for column to sort
 * @returns {number}
 */
const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
};

/**
 * function helper for sorting array of objects
 * @param order {string} - sort direction value
 * @param orderBy {string} - label for column to sort
 * @returns { number}
 */
export const getComparator = (order, orderBy) => {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
};
