export const EXCHANGED = 'Exchanged';
export const BITCOIN = 'Bitcoin';
export const ETHEREUM = 'Ethereum';
export const HISTORY_DATA = 'historyData';
export const LIST = 'LIST';
export const RATE = 'rate';
export const CURRENCY = 'currency';
export const HISTORY = 'history';
export const POST = 'POST';
