import {makeStyles} from '@material-ui/core/styles';
import {InputBase, Select} from '@mui/material';
import {styled} from '@mui/system/';
import Snackbar from '@mui/material/Snackbar';

export const useStyles = makeStyles(() => ({
    equal: {
        position: 'relative',
        left: 8,
    },

    formControl: {
        marginLeft: 13,
    },

    button: {
        color: 'white',
        height: '40px',
        width: '84px',
        borderRadius: '8px',
        margin: '15px 13px ',
    },

    title: {
        marginLeft: 12,
    },

    selectName: {
        fontSize: 14,
    },

    toolbarContainer: {
        height: 160,
        paddingTop: 40,
    },

}));

export const ToolbarInput = styled(InputBase)(({theme}) => ({
    height: '42px',
    width: '160px',
    'label + &': {
        marginTop: theme.spacing(2),
    },
    '& .MuiInputBase-input': {
        borderRadius: '8px',
        position: 'relative',
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '8px 26px 8px 12px',
    },
}));

export const ToolbarSelect = styled(Select)(({theme}) => ({
    height: '41.5px',
    width: '200px',
    borderRadius: 8,

    'label + &': {
        marginTop: theme.spacing(2),
    },

    '& .icon': {
        position: 'relative',
        top: 4,
    },
}));

export const MySnackbar = styled(Snackbar)(({theme}) => ({
    '& .MuiPaper-root': {
        backgroundColor: '#49CD5E',
        color: '#FFFFFF',
        width: 1360,
        height: 48,
        fontFamily: 'Red Hat Display',
        fontSize: 16,
        fontWeight: 700,
        lineHeight: '21px'
    },
    '& .MuiAlert-message': {
        margin: '0 auto'
    }
}));
