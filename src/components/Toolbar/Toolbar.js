import React, {useState} from 'react';
import {Button, FormControl, InputLabel, MenuItem} from '@material-ui/core';
import {BitcoinIcon, EthereumIcon, UsdIcon} from '../../icons';
import {useExchangeCurrencyMutation, useGetCryptosQuery,} from '../../services/cryptoApi';
import {Select} from '@mui/material';
import {BITCOIN, ETHEREUM, EXCHANGED} from '../../constants';
import {MySnackbar, ToolbarInput, ToolbarSelect, useStyles} from './styled';
import {SubmitingNotification} from '../SubmitingNotification';


/**
 * Responsible for submitting data inputted by user
 * @returns {JSX.Element}
 * @constructor
 */
export const Toolbar = () => {
    const [cryptoCurrency, setCryptoCurrency] = useState('');
    const [currency, setCurrency] = useState('');
    const [amount1, setAmount1] = useState('');
    const [amount2, setAmount2] = useState('');
    const [open, setOpen] = useState(false);
    const classes = useStyles();
    const {data} = useGetCryptosQuery();
    const [exchangeCurrency] = useExchangeCurrencyMutation();
    const cryptoPrice = data && data[0][cryptoCurrency];


    const saveHandler = async (e) => {
        e.preventDefault();
        if (cryptoCurrency && currency && amount1 && amount2) {
            await exchangeCurrency({
                id: Date.now(),
                date: Date.now(),
                currencyFrom: cryptoCurrency,
                amount1,
                currencyTo: currency,
                amount2,
                type: EXCHANGED,
            });
            setCryptoCurrency('');
            setCurrency('');
            setAmount1('');
            setAmount2('');
            setOpen(true);
        }

    };

    const handleCloseNotification = () => {
        setOpen(false);
    };

    const handleChangeCrypto = (event) => {
        setCryptoCurrency(event.target.value);
    };

    const handleChangeCurrency = (event) => {
        setCurrency(event.target.value);
        if(cryptoCurrency) {
            setAmount2(amount1 * cryptoPrice);
        }
    };

    const handleAmount1Change = (event) => {
        if (!isNaN(+event.target.value)) {
            if (cryptoCurrency && currency) {
                setAmount2(Number(event.target.value) * cryptoPrice);
            }
            event.target.value !== '' ? setAmount1(Number(event.target.value)) : setAmount1(event.target.value);
        }
    };

    return (
        <div className={classes.toolbarContainer}>
            <form
                onSubmit={saveHandler}
                className={classes}
                noValidate
                autoComplete="off"
            >
                <h2 className={classes.title}>Exchange</h2>
                <FormControl className={classes.formControl} variant="standard">
                    <InputLabel shrink htmlFor="bootstrap-input">
                        Currency from
                    </InputLabel>
                    <Select
                        labelId="demo-customized-select-label"
                        id="demo-customized-select"
                        value={cryptoCurrency}
                        onChange={handleChangeCrypto}
                        input={<ToolbarSelect/>}
                    >
                        <MenuItem value={BITCOIN}>
                            <BitcoinIcon/>{' '}
                            <span className={classes.selectName}>{BITCOIN}</span>
                        </MenuItem>
                        <MenuItem value={ETHEREUM}>
                            <EthereumIcon/>{' '}
                            <span className={classes.selectName}>{ETHEREUM}</span>
                        </MenuItem>
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl} variant="standard">
                    <InputLabel shrink htmlFor="amount1">
                        Amount
                    </InputLabel>
                    <ToolbarInput
                        onChange={handleAmount1Change}
                        value={amount1}
                        id="amount1"
                        type="number"
                    />
                </FormControl>
                <span className={classes.equal}>=</span>
                <FormControl className={classes.formControl} variant="standard">
                    <InputLabel shrink htmlFor="bootstrap-input">
                        Currency to
                    </InputLabel>
                    <Select
                        labelId="demo-customized-select-label"
                        id="demo-customized-select"
                        value={currency}
                        onChange={handleChangeCurrency}
                        input={<ToolbarSelect/>}
                    >
                        <MenuItem value={'USD'}>
                            <UsdIcon/> USD
                        </MenuItem>
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl} variant="standard">
                    <InputLabel shrink htmlFor="amount2">
                        Amount
                    </InputLabel>
                    <ToolbarInput value={amount2} id="amount2"/>
                </FormControl>
                <Button
                    className={classes.button}
                    type="submit"
                    variant="contained"
                    color="primary"
                >
                    Save
                </Button>
                <MySnackbar open={open} autoHideDuration={3000} onClose={handleCloseNotification}>
                    <SubmitingNotification severity="success">
                        Exchange submitted
                    </SubmitingNotification>
                </MySnackbar>
            </form>
        </div>
    );
};
