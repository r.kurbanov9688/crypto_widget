import * as React from 'react';
import {useState} from 'react';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import {Button, FormControl, InputLabel, MenuItem} from '@material-ui/core';
import {Select} from '@mui/material';
import {DatePicker, LocalizationProvider} from '@mui/lab';
import {DatePickerInput, TypeSelect, useStyles} from './styled';

/**
 * Controls filtering by date and type
 * @param setData {function} - function to set filtered data
 * @param data {array} - array of history exchanges
 * @returns {JSX.Element}
 * @constructor
 */
export const DateFilter = ({setData, data}) => {
    const classes = useStyles();
    const [fromDate, setFromDate] = useState(null);
    const [toDate, setToDate] = useState(null);
    const [type, setType] = useState('all');
    const handleChangeType = (e) => {
        setType(e.target.value);
    };

    const handleFilter = () => {
        setData(
            data.filter(
                (el) => {
                    return (
                        el.type === type ||
                        (el.date >= Date.parse(fromDate) && Date.parse(toDate) >= el.date)
                    );

                }
            )
        );
        if (!fromDate && !toDate && type === 'all') {
            setData(data);
        }
    };

    return (
        <div className={classes.root}>
            <h2 className={classes.title}>History</h2>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
                <div className={classes.container}>
                    <DatePicker
                        label="From date"
                        value={fromDate}
                        onChange={(newValue) => {
                            setFromDate(newValue);
                        }}
                        renderInput={(params) => (
                            <DatePickerInput
                                {...params}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        )}
                    />
                    <DatePicker
                        label="To date"
                        value={toDate}
                        onChange={(newValue) => {
                            setToDate(newValue);
                        }}
                        renderInput={(params) => (
                            <DatePickerInput
                                {...params}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        )}
                    />
                    <FormControl className={classes.formControl} variant="standard">
                        <InputLabel shrink htmlFor="bootstrap-input">
                            Type
                        </InputLabel>
                        <Select
                            labelId="demo-customized-select-label"
                            id="demo-customized-select"
                            value={type}
                            onChange={handleChangeType}
                            input={<TypeSelect/>}
                        >
                            <MenuItem value={'all'}>
                                {' '}
                                <span className={classes.selectName}>All</span>
                            </MenuItem>
                            <MenuItem value={'Live Price'}>
                                {' '}
                                <span className={classes.selectName}>Live Price</span>
                            </MenuItem>
                            <MenuItem value={'Exchanged'}>
                                <span className={classes.selectName}>Exchanged</span>
                            </MenuItem>
                        </Select>
                    </FormControl>
                    <Button
                        className={classes.button}
                        variant="outlined"
                        onClick={handleFilter}
                    >
                        Filter
                    </Button>
                </div>
            </LocalizationProvider>
        </div>
    );
};
