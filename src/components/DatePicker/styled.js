import {makeStyles} from '@material-ui/core/styles';
import {Select} from '@mui/material';
import TextField from '@mui/material/TextField';
import {styled} from '@mui/system/';

export const useStyles = makeStyles(() => ({
    root: {
        '& .MuiTextField-root': {
            margin: '16px 6px',
            borderRadius: '8px',
        },
    },
    title: {
        height: '27px',
        width: '63px',
        fontSize: '18px',
        fontWeight: 700,
        lineHeight: '27px',
        marginLeft: 12,
    },
    input: {
        height: '42px',
        width: '160px',
    },
    formControl: {
        marginLeft: 6,
    },
    button: {
        height: '16px',
        width: '40px',
        borderRadius: 8,
        padding: '18px 36px',
        fontSize: '12px',
        color: '#1B31A8',
        borderColor: '#1B31A8',
        marginLeft: 13,
    },
    container: {
        display: 'flex',
        alignItems: 'baseline',
        marginLeft: 6,
    },
}));

export const TypeSelect = styled(Select)(({theme}) => ({
    height: '42px',
    width: '160px',
    borderRadius: 8,

    'label + &': {
        marginTop: theme.spacing(2),
    },
}));

export const DatePickerInput = styled(TextField)(() => ({
    '& .MuiOutlinedInput-root': {
        height: '42px',
        width: '160px',
        borderRadius: 8,
        span: {
            display: 'none',
        },
    },

    '& .MuiInputLabel-root': {
        fontSize: 16,
        fontFamily: 'Red Hat Display',
        fontWeight: 400,
        margin: '-10px -16px',
    },
}));
