import * as React from 'react';
import {useEffect, useState} from 'react';
import {
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TableSortLabel,
} from '@material-ui/core';
import {DateFilter} from './DatePicker/DatePicker';
import {useGetHistoryQuery} from '../services/cryptoApi';
import {UsePagination} from './Pagination/Pagination';
import {EXCHANGED} from '../constants';
import {getComparator, stableSort} from '../utils';

/*
 * The table with history of exchanges
 * @returns {JSX.Element}
 * @constructor
 */
export const HistoryTable = () => {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage] = React.useState(5);
    const {data: history} = useGetHistoryQuery();
    const [filteredData, setFilteredData] = useState(history);
    const count = filteredData ? filteredData.length : 0;
    const [order, setOrder] = useState();
    const [orderBy, setOrderBy] = useState();

    const headCells = [
        {label: 'Date & Time', id: 'date'},
        {label: 'Currency From', id: 'currencyFrom'},
        {label: 'Ammount 1', id: 'amount1'},
        {label: 'Currency To', id: 'currencyTo'},
        {label: 'Ammount 2', id: 'amount2'},
        {label: 'Type', id: 'type'},
    ];

    const emptyRows =
        history &&
        rowsPerPage - Math.min(rowsPerPage, history.length - page * rowsPerPage);

    useEffect(() => {
        setFilteredData(history);
    }, [history]);

    const handleSortRequest = (cellId) => {
        const isAsc = orderBy === cellId && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(cellId);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage - 1);
    };

    return (
        <TableContainer component={Paper}>
            <DateFilter data={history} setData={setFilteredData}/>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow style={{background: '#EEEEEE'}}>
                        {headCells.map((headCell) => (
                            <TableCell
                                key={headCell.id}
                                align={'left'}
                                sortDirection={orderBy === headCell ? order : false}
                            >
                                <TableSortLabel
                                    active={orderBy === headCell.id}
                                    direction={orderBy === headCell.id ? order : 'asc'}
                                    onClick={() => {
                                        handleSortRequest(headCell.id);
                                    }}
                                >
                                    {headCell.label}
                                </TableSortLabel>
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {filteredData &&
                        stableSort(filteredData, getComparator(order, orderBy))
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row, index) => (
                                <TableRow
                                    key={row.date}
                                    style={
                                        index % 2
                                            ? {background: '#EEEEEE'}
                                            : {background: 'white'}
                                    }
                                >
                                    <TableCell component="th" scope="row">
                                        {new Date(row.date).toLocaleString()}
                                    </TableCell>
                                    <TableCell align="left">{row.currencyFrom}</TableCell>
                                    <TableCell align="left">{row.amount1}</TableCell>
                                    <TableCell align="left">{row.currencyTo}</TableCell>
                                    <TableCell align="left">{row.amount2.toFixed(5)}</TableCell>
                                    <TableCell
                                        style={{
                                            color: row.type === EXCHANGED ? '#6368DF' : '#5DBE7E',
                                        }}
                                        align="left"
                                    >
                                        {row.type}
                                    </TableCell>
                                </TableRow>
                            ))}
                    {emptyRows > 0 && (
                        <TableRow style={{height: 53 * emptyRows}}>
                            <TableCell colSpan={6}/>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
            <UsePagination
                count={Math.ceil(count / rowsPerPage)}
                onChange={handleChangePage}
            />
        </TableContainer>
    );
};
