import {styled} from '@mui/material/styles';
import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
    buttonContainer: {
        display: 'flex',
    },

    arrow: {
        marginLeft: 8,
    },

    button: {
        border: 'none',
        height: 40,
        width: 40,
        backgroundColor: 'white',
        marginTop: 6,
    },

    page: {
        height: '40px',
        width: '40px',
        borderRadius: '8px',
        border: 'none',
        margin: 6,
    },
}));

export const List = styled('ul')({
    listStyle: 'none',
    padding: 0,
    margin: 0,
    display: 'flex',
});
