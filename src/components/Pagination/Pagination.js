import * as React from 'react';
import usePagination from '@mui/material/usePagination';
import {ArrowIcon} from '../../icons';
import {List, useStyles} from './styled';

/**
 * Pagination
 * @param count {number} - amount of pages
 * @param onChange {function} - function which handles page changing
 * @returns {JSX.Element}
 * @constructor
 */
export const UsePagination = ({count, onChange}) => {
    const {items} = usePagination({
        count: count,
        hidePrevButton: true,
        onChange: onChange,
    });
    const classes = useStyles();
    return (
        <nav>
            <List>
                {items.map(({page, type, selected, ...item}, index) => {
                    let children;
                    if (type === 'start-ellipsis' || type === 'end-ellipsis') {
                        children = '…';
                    } else if (type === 'page') {
                        children = (
                            <button
                                className={classes.page}
                                type="button"
                                style={{
                                    fontWeight: selected ? 'bold' : undefined,
                                    padding: selected ? '8px' : undefined,
                                    backgroundColor: selected ? 'black' : 'white',
                                    color: selected ? 'white' : undefined,
                                }}
                                {...item}
                            >
                                {page}
                            </button>
                        );
                    } else {
                        children = (
                            <button className={classes.button} type="button" {...item}>
                                <div className={classes.buttonContainer}>
                                    <span>Next</span>
                                    <span className={classes.arrow}>
                                        <ArrowIcon/>
                                    </span>
                                </div>
                            </button>
                        );
                    }

                    return <li key={index}>{children}</li>;
                })}
            </List>
        </nav>
    );
};
