import * as React from 'react';
import MuiAlert from '@mui/material/Alert';

export const SubmitingNotification = React.forwardRef(function Alert(props, ref) {

    return <MuiAlert style={{
        backgroundColor: 'red'
    }
    } elevation={6} ref={ref} {...props} icon={false}/>;
});


