import './App.css';
import {Toolbar} from './components/Toolbar/Toolbar';
import {HistoryTable} from './components/HistoryTable';
import {Provider} from 'react-redux';
import {store} from './store/store';
import {createTheme, ThemeProvider} from '@material-ui/core/styles';
import {Paper} from '@material-ui/core';

const theme = createTheme({
    palette: {
        primary: {
            main: '#49CD5E',
        },
    },
    typography: {
        fontFamily: 'Red Hat Display',
    },
});

/**
 *  The root element of the application
 * @returns {JSX.Element}
 * @constructor
 */
function App() {
    return (
        <div className="App">
            <Provider store={store}>
                <ThemeProvider theme={theme}>
                    <Paper className="container">
                        <Toolbar/>
                        <HistoryTable/>
                    </Paper>
                </ThemeProvider>
            </Provider>
        </div>
    );
}

export default App;
