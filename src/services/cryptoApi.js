import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import {CURRENCY, HISTORY, HISTORY_DATA, LIST, POST, RATE,} from '../constants';

const baseUrl = process.env.REACT_APP_BASE_URL;

/**
 * Manages requests to API
 * @type {Api<(args: (string | FetchArgs), api: BaseQueryApi, extraOptions: {}) => MaybePromise<QueryReturnValue<unknown, {status: number, data: unknown} | {status: "FETCH_ERROR", data?: undefined, error: string} | {status: "PARSING_ERROR", originalStatus: number, data: string, error: string} | {status: "CUSTOM_ERROR", data?: unknown, error: string}, FetchBaseQueryMeta>>, {getCryptos: *, getHistory: *, exchangeCurrency: *}, string, string, typeof coreModuleName> | Api<(args: (string | FetchArgs), api: BaseQueryApi, extraOptions: {}) => MaybePromise<QueryReturnValue<unknown, {status: number, data: unknown} | {status: "FETCH_ERROR", data?: undefined, error: string} | {status: "PARSING_ERROR", originalStatus: number, data: string, error: string} | {status: "CUSTOM_ERROR", data?: unknown, error: string}, FetchBaseQueryMeta>>, {getCryptos: *, getHistory: *, exchangeCurrency: *}, string, string, any>}
 */
export const cryptoApi = createApi({
    tagTypes: [HISTORY_DATA],
    reducerPath: CURRENCY,
    baseQuery: fetchBaseQuery({baseUrl}),
    endpoints: (builder) => ({
        getCryptos: builder.query({
            query: () => RATE,
        }),
        getHistory: builder.query({
            query: () => HISTORY,
            providesTags: (result) =>
                result
                    ? [
                        ...result.map(({id}) => ({type: HISTORY_DATA, id})),
                        {type: HISTORY_DATA, id: LIST},
                    ]
                    : [{type: HISTORY_DATA, id: LIST}],
        }),
        exchangeCurrency: builder.mutation({
            query: (body) => ({
                url: HISTORY,
                method: POST,
                body,
            }),
            invalidatesTags: [{type: HISTORY_DATA, id: LIST}],
        }),
    }),
});

export const {
    useGetCryptosQuery,
    useGetHistoryQuery,
    useExchangeCurrencyMutation,
} = cryptoApi;
