import {configureStore} from '@reduxjs/toolkit';
import {cryptoApi} from '../services/cryptoApi';

/**
 * Creates redux toolkit store
 * @type {EnhancedStore<{}, AnyAction, MiddlewareArray<[...ExcludeFromTuple<[ThunkMiddlewareFor<{}, {thunk: true, immutableCheck: true, serializableCheck: true}>], never>, ...*]>>}
 */
export const store = configureStore({
    reducer: {
        [cryptoApi.reducerPath]: cryptoApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(cryptoApi.middleware),
});
